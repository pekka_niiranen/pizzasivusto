
var pizzApp = angular.module('pizzApp', ['ngRoute']);

pizzApp.config(function($routeProvider){

	$routeProvider

	//home
	.when('/',{
		templateUrl : 'pages/home.html',
		controller : 'mainController'

	})

	.when('/about',{
		templateUrl : 'pages/about.html',
		controller : 'aboutController'

	})

	.when('/contact',{
		templateUrl : 'pages/contact.html',
		controller : 'contactController'

	});


});

pizzApp.controller('mainController', function($scope){

	$scope.message = "Lista maailman parhaista pitsoista!";

});

pizzApp.controller('aboutController', function($scope){

	$scope.message = 'Tietoja sivustosta';

});

pizzApp.controller('contactController', function($scope){

	$scope.message = 'Täältä löytyvät tekijän yhteystiedot';

});

pizzApp.controller('pizzaListController', ['$scope', '$http', function($scope, $http){
   $http.get('pizzat/pizzat.json').success(function(data){
	$scope.pizzat = data;
    });

$scope.orderProp = 'name';

}]);



